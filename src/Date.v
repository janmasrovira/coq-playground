From mathcomp Require Import all_ssreflect.

(* https://datatracker.ietf.org/doc/html/rfc3339#section-5.6 *)
Record Time : Type :=
  mk_time {
      hour : nat
      ; minute : nat
    }.


Inductive Token : Type :=
  | number : nat -> Token
  | T : Token
  | space : Token
  | minus : Token.

Inductive FormatToken : Type :=
  | h : FormatToken
  | dash : FormatToken
  | m : FormatToken.

Definition RawFormat : Type := seq FormatToken.

Definition eqft (a b : FormatToken) : bool :=
  match a , b with
    | h , h => true
    | dash , dash => true
    | m , m => true
    | _ , _ => false
  end.

Lemma tok_eqP (a b : FormatToken) : reflect (a = b) (eqft a b).
Proof using Type.
  apply /(iffP idP) => e.
  destruct a; destruct b => //.
  rewrite e. destruct b => //.
Qed.


Definition tok_eqMixin := Equality.Mixin tok_eqP.
Canonical tok_eqType := @Equality.Pack FormatToken tok_eqMixin.

About permutation.
Record Format : Type :=
  mk_format {
      raw_format : RawFormat
    ; is_format : perm_eq raw_format [:: h; m]
    }.

Definition String : Type := seq Token.
Definition geth (t : Time) : nat := hour t.

Definition format (d : Time) : String :=
  [:: number (hour d); minus; number (minute d) ].

Definition parse (s : String) : option Time :=
  match s with
    | [:: number hour; minus; number min] => Some (mk_time hour min)
    | _ => None
  end.

Print option.
About Some_injective.

Lemma recons t : t = {| hour := hour t; minute := minute t |}.
Proof using Type.
  destruct t => //.
Qed.

Lemma thm (t : Time) : Some t = parse (format t).
Proof using Type.
  rewrite /format //=.
  congr Some.
  apply: recons.
Qed.
