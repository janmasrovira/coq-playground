From mathcomp Require Import all_ssreflect.

Load Base.

Set Diffs "on".

Section  DPLL.

Variable V : nat.
Definition Var := ordinal V.

(* A literal is a tuple <var, b>, where b = true iff the literal is positive *)
Definition Lit : Type := bool * Var.

Definition Clause : Type := seq Lit.
Definition CNF : Type := seq Clause.
Definition Val : Type := {ffun Var -> bool}.

Section Eval.
  Variable val : Val.

  Definition eval_lit (l : Lit) : bool :=
    match l with
    | (pos , v) => (val v) == pos
    end.

  Definition eval_clause : Clause -> bool :=
    has eval_lit.

  Definition eval_cnf : CNF -> bool :=
    all eval_clause.
End Eval.

Definition Sat (cnf : CNF) : Prop :=
  ex (fun val => eval_cnf val cnf).

Definition Unsat (cnf : CNF) : Prop :=
  forall val, ~~ (eval_cnf val cnf).

Definition negl (l : Lit) : Lit :=
  match l with
    | (p , v) => (negb p, v)
  end.

Definition eql (a b : Lit) : bool :=
  match a , b with
    | (v1 , p1) , (v2 , p2) => (v1 == v2) && (p1 == p2)
  end.

Definition eqll l : eql l l.
Proof using Type.
  elim: l => [a b //=].
  apply: andb_intro; apply: eq_refl.
Qed.

Definition eqlP a b : reflect (a = b) (eql a b).
Proof using Type.
  apply: (iffP idP).
  elim: a => [a1 a2]; elim b => [b1 b2 /andP[/eqP-> /eqP->] //].
  move => [->]. apply: eqll.
Qed.

Definition in_clause (l : Lit) : Clause -> bool :=
  has (eql l).

Lemma in_clause_Pf l c
  : reflect (~~ (in_clause l c)) (all (fun lit => negb (eql l lit)) c).
Proof using Type.
  apply: (iffP idP).
  elim: c => [//|c cs ih /andP[h t]].
  rewrite /in_clause /has Bool.negb_orb. apply: andb_intro. apply: h.
  apply: (ih t).
  elim: c => [//|c cs ih].
  rewrite Bool.negb_orb => /andP[a b]. apply: andb_intro. apply: a.
  apply: ih b.
Qed.

Lemma in_clauseP l c : reflect (in_clause l c) (l \in c).
Proof using Type.
  apply: (iffP idP);
  elim: c => [//|d ds ih].
  rewrite in_cons => /orP[[/eqP->]|e].
  apply: orb_intro_l. apply: eqll.
  apply: orb_intro_r. apply (ih e).
  move=> /orP-[/eqlP->|k]. rewrite in_cons eq_refl //=.
  apply: orb_intro_r. apply: ih k.
Qed.

Definition Pure_clause (l : Lit) : Clause -> bool :=
 all (fun lit => ~~ (eql (negl l) lit)).

(* A literal is pure if its negation does not appear in any clause. *)
Definition Pure_cnf (l : Lit) : CNF -> bool :=
 all (Pure_clause l).

Definition Trivial_clause_lit (cl : Clause) (l : Lit) : bool :=
  in_clause l cl && in_clause (negl l) cl.

Definition Trivial_clause (cl : Clause) : Prop :=
  ex (Trivial_clause_lit cl).

Definition Trivial_clause_dec (cl : Clause) : bool :=
  has (Trivial_clause_lit cl) cl.

Lemma Trivial_clauseP cl : reflect (Trivial_clause cl) (Trivial_clause_dec cl).
Proof using Type.
  apply: (iffP idP).
  move=> /hasP[k _]. exists k => //.
  move=> [k u]. apply/hasP.
  exists k.
  have k_in_cl : k \in cl.
  move/andP: u. move => [[/in_clauseP->] //].
  apply: k_in_cl. apply: u.
Qed.

Definition Unit_clause (l : Lit) (cl : Clause) : bool :=
  cl == [:: l].

Definition Unit_in_cnf (l : Lit) : CNF -> bool :=
  has (Unit_clause l).

(* Eliminating a pure literal consists in removing all the clauses that contain it *)
Definition Pure_elim {l} {cnf} (p : Pure_cnf l cnf) : CNF :=
  filter (fun cl => (all (fun lit => negb (eql l lit)) cl)) cnf.

Definition Lit_negl_elim l : Clause -> Clause :=
  (filter (fun lit => ~~ (eql (negl l) lit))).

(* Eliminates all clauses which contain the given literal *)
Definition Lit_elim l cnf : CNF :=
  map (Lit_negl_elim l) (filter (fun cl => ~~ (in_clause l cl)) cnf).

(* Adapts a valuation so that it satisfies the specified literal while not
   changing anything else *)
Definition adapt (val : Val) (lit : Lit) : Val := finfun (fun v =>
  match lit with
    | (p , u) => if u == v then p else val v
  end).

Lemma adapt_eq (val : Val) (lit : Lit) : eval_lit (adapt val lit) lit.
Proof using Type.
  case: lit => a p //=.
  rewrite ffunE.
  rewrite !eq_refl //.
Qed.

Lemma adapt_neq (val : Val) (t l : Lit) :
  (l.2 != t.2) -> eval_lit (adapt val l) t = eval_lit val t.
Proof using Type.
  move: t l.
  move => [v1 p1] [v2 p2] /negPf //=.
  rewrite ffunE.
  move => -> //.
Qed.

(* Lemma eval_cnf_cons val cl cnf : *)
(*   reflect (eval_cnf val (cons cl cnf)) (eval_clause val cl && eval_cnf val cnf). *)
(* Proof using Type. *)
(*   apply: (iffP idP) => [/andP[ecl ecnf] |]. *)
(*   rewrite /eval_cnf /all ecl //. *)
(*   rewrite /eval_cnf /all => [/andP[-> //]]. *)
(* Qed. *)

Lemma filter_sat f cnf : Sat cnf -> Sat (filter f cnf).
  move => [val e].
  exists val.
  move: e. elim: cnf => [//|cl cnf ih /andP [ecl ecnf]].
  rewrite /filter.
  case: (f cl). rewrite /eval_cnf/all ecl //=.
  apply: (ih ecnf).
  apply: (ih ecnf).
Qed.

Lemma Pure_sat l cnf (p : Pure_cnf l cnf) : Sat cnf -> Sat (Pure_elim p).
Proof using Type. apply: filter_sat. Qed.

Lemma eval_clause_cons l ls val
  : eval_clause val (l :: ls) = eval_lit val l || eval_clause val ls.
Proof using Type. by []. Qed.

Lemma Pure_cl_sat l cl val (p : Pure_clause l cl) (p1 : eval_clause val cl)
  : eval_clause (adapt val l) cl.
Proof using Type.
  move: l p p1. move=> [pos1 v1].
  elim: cl => [//|[pos2 v2] // hs ih /andP[neq pure_tail] ecl].
  rewrite /eval_clause/has in ecl.
  Check (ih pure_tail).
  case/orP: ecl => [head |h_tail]; last first; rewrite eval_clause_cons.
  rewrite (ih pure_tail h_tail) Bool.orb_true_r //.
  rewrite Bool.negb_andb in neq.
  case/boolP: (v1 == v2) => [veq|vneq].
  rewrite //= ffunE veq.
  rewrite veq //= in neq.
  apply: orb_intro_l.
  rewrite negb_neq Bool.orb_false_r // in neq.
  rewrite (adapt_neq val (pos2 , v2) (pos1 , v1) vneq) head //=.
Qed.

Lemma eval_cnf_cons l ls val
  : eval_cnf val (l :: ls) = eval_clause val l && eval_cnf val ls.
Proof using Type. by []. Qed.

Lemma adapt_in_clause val l c : in_clause l c -> eval_clause (adapt val l) c.
Proof using Type.
  elim: c => [//|l1 ls ih].
  rewrite /has => [/orP[/eqlP -> |in_tail]].
  apply: orb_intro_l. apply: adapt_eq.
  apply: orb_intro_r. apply: ih in_tail.
Qed.

Lemma adapt_negl_not_in_clause {val} l c
  : ~~ (in_clause (negl l) c) -> eval_clause val c -> eval_clause (adapt val l) c.
Proof using Type.
  move: l c. move=> [v1 s1] c.
  elim: c => [//|[s2 v2] ds ih].
  move => /in_clause_Pf/andP[neqld /in_clause_Pf-neqds].
  rewrite eval_clause_cons. move => /orP[vd|u].
  rewrite /eval_lit eq_sym in vd. move: vd neqld. move=> /eqP ->.
  rewrite /= Bool.negb_andb ffunE => m.
  case: ifP => [/eqP n|n].
  rewrite n eq_refl //= in m.
  rewrite negb_neq // Bool.orb_false_r in m. rewrite m //=.
  rewrite eq_refl //. apply: orb_intro_r. apply: (ih neqds u).
Qed.

Lemma pure_cnf_tail {l} {c} {cs} : Pure_cnf l (c :: cs) -> Pure_cnf l cs.
Proof using Type. move => /andP[a b //]. Qed.

Lemma Pure_sat_lemma val l cnf (p : Pure_cnf l cnf)
  : eval_cnf val (Pure_elim p) -> eval_cnf (adapt val l) cnf.
Proof using Type.
  move: p. elim: cnf => [//|c cs ih].
  rewrite /Pure_elim /filter.
  case/boolP: (all (fun lit : Lit => ~~ eql l lit) c) => [_ pure | /negPf h1].
  rewrite !eval_cnf_cons => /andP[val_c val_cs].
  move: pure. rewrite /Pure_cnf /all => /andP[pure_c pure_cs].
  apply: andb_intro. apply: Pure_cl_sat => //.
  apply: ih. apply: val_cs.
  case/boolP: (in_clause l c) => [l_in_c|/in_clause_Pf r].
  move => [pure eval]. apply: andb_intro.
  apply: adapt_in_clause l_in_c.
  apply: (ih (pure_cnf_tail pure) eval).
  rewrite h1 in r => //.
Qed.

Lemma Pure_sat2 l cnf (p : Pure_cnf l cnf) : Sat (Pure_elim p) -> Sat cnf.
Proof using Type.
  move => [val e]. exists (adapt val l). apply: Pure_sat_lemma e.
Qed.

Lemma Lit_elim_cons l c cs
  : Lit_elim l (c :: cs) =
    if in_clause l c then Lit_elim l cs else Lit_negl_elim l c :: Lit_elim l cs.
Proof using Type.
  rewrite /Lit_elim.
  case: ifP => cc;
  rewrite filter_cons cc //=.
Qed.

Lemma eval_filter_clause {val} {f} {cl}
      : eval_clause val (filter f cl) -> eval_clause val cl.
Proof using Type.
  elim: cl => [//|c cs ih].
  rewrite /filter. case: ifP => [fc//= |nfc k].
  move=> /orP[-> //=|b]. apply: (orb_intro_r (ih b)).
  apply: orb_intro_r (ih k).
Qed.

Lemma Lit_elim_lemma l c : ~~ (in_clause (negl l) (Lit_negl_elim l c)).
Proof using Type.
  elim: c => [//|d ds].
  rewrite /Lit_negl_elim/filter.
  case: ifP => e e1. rewrite Bool.negb_orb. apply: andb_intro. apply: e.
  apply: e1. apply: e1.
Qed.

Lemma Unit_sat_lemma val l cnf
  : eval_cnf val (Lit_elim l cnf) -> eval_cnf (adapt val l) cnf.
Proof using Type.
  elim: cnf => [//|c cs ih].
  rewrite Lit_elim_cons.
  case: ifP => [i ics|i]. apply: andb_intro. apply: adapt_in_clause i.
  apply: ih ics.
  rewrite eval_cnf_cons => /andP[h hs].
  apply: andb_intro.
  apply: eval_filter_clause.
  apply: (adapt_negl_not_in_clause l (Lit_negl_elim l c)).
  apply: Lit_elim_lemma. apply: h.
  apply: ih hs.
Qed.

Lemma Unit_sat l cnf (u : Unit_in_cnf l cnf) : Sat (Lit_elim l cnf) -> Sat cnf.
Proof using Type.
  move => [val e]. exists (adapt val l). apply: Unit_sat_lemma e.
Qed.

Definition Trivial_elim : CNF -> CNF
  := filter (fun c => ~~ (Trivial_clause_dec c)).

Lemma Trivial_elim_cons c cs :
  Trivial_elim (c :: cs) = if Trivial_clause_dec c then Trivial_elim cs else c :: Trivial_elim cs.
Proof using Type.
  rewrite /Trivial_elim .
  case: ifP => [//= -> //= |//= -> //=].
Qed.

Lemma eval_lit_or_negl val l : eval_lit val l || eval_lit val (negl l).
Proof using Type.
  move: l. move=> [s v].
  rewrite /negl/eval_lit.
  case: (val v); case: s => //.
Qed.

Lemma eval_Trivial_clause {val} {c} : Trivial_clause c -> eval_clause val c.
Proof using Type.
  move => [d /andP[/in_clauseP-di /in_clauseP-ni]].
  apply /hasP.
  case/orP: (eval_lit_or_negl val d) => ev.
  exists d => //. exists (negl d) => //.
Qed.

Lemma Trivial_elim_sat_lemma {val} {cnf}
  : eval_cnf val (Trivial_elim cnf) -> eval_cnf val cnf.
Proof using Type.
  elim: cnf => [//|c cs ih].
  rewrite Trivial_elim_cons.
  case: ifP => [/Trivial_clauseP tc ecs|_ /andP[u i]]; apply: andb_intro.
  apply: (eval_Trivial_clause tc). apply: ih ecs.
  apply: u. apply: ih i.
Qed.

Lemma Trivial_elim_sat cnf :
  Sat (Trivial_elim cnf) -> Sat cnf.
Proof using Type.
  move => [v a]. exists v. apply: (Trivial_elim_sat_lemma a).
Qed.

Definition is_sat (cnf : CNF) : bool :=
  [exists val, eval_cnf val cnf].

Theorem solve_sat (cnf : CNF) : Sat cnf \/ Unsat cnf.
Proof using Type.
  case/boolP: (is_sat cnf) => [/existsP-yes|/existsPn-no].
  apply: or_introl yes.
  apply: or_intror no.
Qed.
End DPLL.
