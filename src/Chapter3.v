Module Chapter3.

From mathcomp Require Import all_ssreflect.
(* Set Implicit Arguments. *)
(* Unset Strict Implicit. *)
(* Unset Printing Implicit Defensive. *)

About prime.

(* Lemma test : forall xy, prime xy.1 -> odd xy.2 -> 2 < xy.2 + xy.1. *)
(* Proof using Type. *)
(*   (* move => [x y] /= /prime_gt1 prime_x. *) *)
(*     move=> [x [//|y]] /= /prime_gt1-x_gt1 _. *)
(*     apply: ltn_addl x_gt1. *)

Lemma eq_to_eqn (a b : nat) : a = b -> a == b.
Proof using Type.
  move => zb //=.
  elim: zb.
  apply: eq_refl.
Qed.

Lemma eq_S (a b : nat) : a = b -> S a = S b.
Proof using Type.
  move <-. by [].
Qed.

Lemma eq_Pred (a b : nat) : S a == S b -> a == b.
Proof using Type.
  move => //=.
Qed.

Lemma eqn_to_eq (a b : nat) : a == b -> a = b.
Proof using Type.
  elim: a b => [|a ih [//|z]]; last first.
  move /eq_Pred/ih/eq_S.
    by [].
    case. by [].
    move => k //.
Qed.

End Chapter3.
