From mathcomp Require Import all_ssreflect.

Lemma negb_neq {a b : bool} : (~~ a != b) = (a == b).
Proof using Type.
  elim: a; elim: b => //=.
Qed.

Lemma negb_neqP {a b : bool} : reflect (a == b) (~~ a != b) .
Proof using Type. apply: (iffP idP); rewrite negb_neq //. Qed.

Lemma orb_intro_l {a b : bool} : a -> a || b.
Proof using Type.
  elim: a => //=.
Qed.

Lemma orb_intro_r {a b : bool} : b -> a || b.
Proof using Type.
  elim: a => //=.
Qed.

Lemma andb_intro {a b : bool} : a -> b -> a && b.
Proof using Type. elim a; elim b => //. Qed.

Lemma filter_cons {A} f (x : A) s
  : filter f (x :: s) = if f x then x :: filter f s else filter f s.
Proof using Type. by []. Qed.

Lemma orb_expand {a b : bool} : a || b = a || (~~a && b).
Proof using Type. elim a; elim b => //. Qed.
