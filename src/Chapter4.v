From mathcomp Require Import all_ssreflect.
(* Set Implicit Arguments. *)
(* Unset Strict Implicit. *)
(* Unset Printing Implicit Defensive. *)

Load Chapter3.
Import Chapter3.

Lemma leq_total m n : (m <= n) || (m >= n).
Proof using Type.
  rewrite -implyNb.
  apply/implyP.
  rewrite -ltnNge.
  apply: ltnW.
Qed.

(* neq_ltn *)

(* Lemma leq_eqVlt m n : (m <= n) = (m == n) || (m < n). *)
(* Lemma ltn_neqAle m n : (m < n) = (m != n) && (m <= n). *)

Lemma eqn_dec (m n : nat) : (m == n) || (m != n).
Proof using Type.
  case: (m == n) => [//|//].
Qed.

Lemma orbt b : b || true.
Proof using Type.
  case: b => [//|//].
Qed.

Lemma orbf b : b || false = b.
Proof using Type.
  case: b => [//|//].
Qed.

Lemma andbf b : b && false = false.
Proof using Type.
  case: b => [//|//].
Qed.

Lemma neq_sym (a b : nat) : (a != b) -> (b != a).
Proof using Type.
  rewrite (eq_sym) //.
Qed.

Lemma compare m n : (m < n) || (m == n) || (m > n).
Proof using Type.
  rewrite !ltn_neqAle.
  case/orP: (eqn_dec m n) => [ -> | neq].
  case/orP: (leq_total m n) => [ab | ba].
  rewrite orbt //.
    by [].
  case/orP: (leq_total m n) => [ab | ba].
  rewrite neq ab //.
  rewrite ba.
  rewrite (eq_sym n m) neq //=.
  by rewrite orbt.
Qed.

Lemma compare2 m n : (m < n) || (m == n) || (m > n).
Proof using Type.
  case: (ltngtP n m) => [//|//|//].
Qed.

Lemma maxn_either m n : (maxn m n == m /\ n < m) \/ (maxn m n == n /\ m <= n).
Proof using Type.
  case/orP: (compare m n).
  case/orP => [e | /eqn_to_eq ->].
  apply: or_intror.
  rewrite /maxn e eq_refl //.
  apply: conj => //. rewrite ltnW //.
  apply: or_intror. apply: conj.
  rewrite /maxn ltnn eq_refl //. apply: leqnn.
  move=> /ltn_geF-lt.
  apply: or_introl.
  rewrite /maxn ltn_neqAle lt andbf eq_refl //. apply: conj => //.
  rewrite ltnNge lt //.
Qed.

Lemma maxn_either2 m n : (maxn m n == m /\ n < m) \/ (maxn m n == n /\ m <= n).
Proof using Type.
  case: (ltngtP m n) => [_ | _ | ->].
  apply: or_intror => //=.
  apply: or_introl => //=.
  apply: or_intror => //=.
Qed.

Lemma em (a : bool) : a \/ ~~ a.
Proof using Type.
  case: a.
  apply: or_introl => //.
  apply: or_intror => //.
Qed.

Lemma leq_max a b c :
  (a <= maxn b c) = (a <= b) || (a <= c).
Proof using Type.
  case: (maxn_either b c) => [[/eqn_to_eq ->] cb | [/eqn_to_eq ->] bc].
  case: (em (a <= c)) => [ac | /negPf ->].
  case: (em (a <= b)) => [-> //= | /negP-nab].
  contradict nab. apply (leq_trans ac (ltnW cb)).
  rewrite orbf //.
  case: (em (a <= b)) => [ab | /negPf-> //].
  rewrite (leq_trans ab bc) orbt //.
Qed.

Lemma leq_max2 a b c :
  (a <= maxn b c) = (a <= b) || (a <= c).
Proof using Type.
  case: (maxn_either b c) => [[/eqn_to_eq ->] cb | [/eqn_to_eq ->] bc].
  case: (a <= c) / boolP => [ ac | ] .
  case: (a <= b) / boolP => [ //= | /negP-nab].
  contradict nab. apply (leq_trans ac (ltnW cb)).
  rewrite orbf //.
  case: (a <= b) / boolP => [ ab | //].
  rewrite (leq_trans ab bc) orbt //.
Qed.

(* Lemma dvdn_fact m n : 0 < m <= n -> m %| n`!. *)
(* Proof using Type. *)
(* move/andP => [[z le]]. *)
(* Locate "%|". *)

Lemma dvdn_fact m n : 0 < m <= n -> m %| n`!.
Proof using Type.
  case: m => m //=.
  elim: n => [//= | n IHn].
  rewrite ltnS leq_eqVlt.
  case/orP=> [/eqP -> | /IHn].
  apply: dvdn_mulr => //=.
  apply: dvdn_mull.
Qed.

Lemma prime_above m : exists2 p, m < p & prime p.
Proof using Type.
  have /pdivP[p pr_p p_dv_m1]: 1 < m`! + 1.
    by rewrite addn1 ltnS fact_gt0.
    exists p => //.
    rewrite ltnNge.
    move: p_dv_m1.
    apply: contraL.
    move => p_le_m.
    by rewrite dvdn_addr ?dvdn_fact ?prime_gt0 // gtnNdvd ?prime_gt1.
Qed.

Lemma leq_max3 m n1 n2 : (m <= maxn n1 n2) = (m <= n1) || (m <= n2).
Proof using Type.
  have th_sym x y : y <= x -> (m <= maxn x y) = (m <= x) || (m <= y).
  move=> le_yx; rewrite (maxn_idPl le_yx) orb_idr // => le_my.
    by apply: leq_trans le_my le_yx.
      by case/orP: (leq_total n2 n1) => /th_sym; last rewrite maxnC orbC.
Qed.

Lemma leq_max4 m n1 n2 : (m <= maxn n1 n2) = (m <= n1) || (m <= n2).
Proof using Type.
  wlog le_n21: n1 n2 / n2 <= n1 => [th_sym|].
    by case/orP: (leq_total n2 n1) => /th_sym; last rewrite maxnC orbC.
    rewrite (maxn_idPl le_n21) orb_idr // => le_mn2.
      by apply: leq_trans le_mn2 le_n21.
Qed.

Lemma leq_max5 m n1 n2 : (m <= maxn n1 n2) = (m <= n1) || (m <= n2).
Proof using Type.
  wlog le_n21: n1 n2 / n2 <= n1 => [th_sym|].
    by case/orP: (leq_total n2 n1) => /th_sym; last rewrite maxnC orbC.
      by rewrite (maxn_idPl le_n21) orb_idr // => /leq_trans->.
Qed.

Definition edivn_rec d : nat -> nat -> nat * nat :=
  fix loop m q := if m - d is m'.+1 then loop m' q.+1 else (q, m).

Definition edivn m d : nat * nat := if d > 0 then edivn_rec d.-1 m 0 else (0, m).
Check edivn.

Lemma edivn_recE d m q :
  edivn_rec d m q = if m - d is m'.+1 then edivn_rec d m' q.+1 else (q, m).
Proof using Type.
    by case: m.
Qed.

Lemma edivnP m d (ed := edivn m d) :
  ((d > 0) ==> (ed.2 < d)) && (m == ed.1 * d + ed.2).
Proof using Type.
  case: d => [//=|d /=] in ed *.
  rewrite -[edivn m d.+1]/(edivn_rec d m 0) in ed *.
  rewrite -[m]/(0 * d.+1 + m).
  (* rewrite -[m]add0n -[0](@mul0n d.+1). *)
  (* elim: m {-2}m 0 (leqnn m) @ed => [[]//=|n IHn [//=|m]] q le_mn. *)
  elim: m {-2}m 0 (leqnn m) @ed => [[] //=|n IHn [//=|m]] q le_mn.
  rewrite edivn_recE subn_if_gt; case: ifP => [le_dm|lt_md]; last first.
    by rewrite /= ltnS ltnNge lt_md eqxx.
    have /(IHn _ q.+1) : m - d <= n by rewrite (leq_trans (leq_subr d m)).
      by rewrite /= mulSnr -addnA -subSS subnKC.
Qed.

(* Exercise 12 *)
Lemma iffP_lr (P : Prop) (b : bool) : (P -> b) -> (b -> P) -> reflect P b.
Proof using Type.
  case: b => f t //=.
  apply: ReflectT (t _) => //.
  apply: ReflectF => /f-pp //=.
Qed.

Lemma iffP_rl (P : Prop) (b : bool) : reflect P b -> ((P -> b) /\ (b -> P)).
Proof using Type.
  case => //=.
Qed.

(* Exercise 13 *)
Lemma eqnP {n m : nat} : reflect (n = m) (n == m).
Proof using Type.
  apply: (iffP idP).
  apply: eqn_to_eq.
  apply: eq_to_eqn.
Qed.

(* Exercise 14 *)
Lemma nat_inj_eq T (f : T -> nat) x y :
  injective f -> reflect (x = y) (eqn (f x) (f y)).
Proof using Type.
  move=> f_inj.
  apply: (iffP eqnP).
  by move /f_inj.
  by move <-.
Qed.

(* Exercise 15 *)
Lemma maxn_idPl m n : reflect (maxn m n = m) (m >= n).
Proof using Type.
  apply: iffP_lr.
  case: (maxn_either m n) => [ [/eqP ->] /ltnW // | [/eqP ->] _ -> ].
  apply: leqnn.
  case: (maxn_either m n) => [[/eqP -> //] | [/eqP->] mn nm].
  apply: anti_leq. rewrite mn nm //=.
Qed.

Lemma maxn_idPl2 m n : reflect (maxn m n = m) (m >= n).
Proof using Type.
    by rewrite -subn_eq0 -(eqn_add2l m) addn0 -maxnE; apply: eqP.
Qed.
