From mathcomp Require Import all_ssreflect.

Definition relation A := A -> A -> Type.


Record Graph (V : Type) (R : relation V) : Type :=
  graph{}.

Inductive Vertices : Type :=
  | V1 : Vertices
  | V2 : Vertices
  | V3 : Vertices.

Definition Edges : relation Vertices :=
  fun v1 v2 => match v1 , v2 with
                | V1 , _ => True
                | _ , _ => False
            end.

Inductive Path V (E : relation V) (origin : V) (dest : V) : Type :=
  | edge : E origin dest -> Path V E origin dest
  | step : forall u, E origin u -> Path V E u dest -> Path V E origin dest.

Lemma v1v3 : Path Vertices Edges V1 V3.
Proof.
  apply: edge => //.
Qed.

