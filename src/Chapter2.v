From mathcomp Require Import all_ssreflect.
(* Set Implicit Arguments. *)
(* Unset Strict Implicit. *)
(* Unset Printing Implicit Defensive. *)

About eq.

Definition type : Type := Type.

Lemma lem1 : 1+1+1 = 3.
Proof using.
    by [].
Qed.

Lemma lem2 (b : bool) : negb (negb b) = b.
  (* Proof using Type means: Use only section variables occurring in the statement. *)
  Proof using Type.
    case: b.
    by [].
    by [].
  Qed.

Lemma leqn0 n : (n <= 0) = (n == 0).
Proof using Type.
  case: n.
    by [].
    by [].
Qed.

Fixpoint iter {A : Type} (f : A -> A) (a : nat) (b : A) : A :=
  match a with
    | 0 => b
    | (S a) => iter f a (f b)
  end.

(* Exercise 2 *)
Definition addn_iter (a b : nat) : nat := iter S a b.

(* Exercise 3 *)
Definition muln_iter (a b : nat) : nat := iter (addn a) b 0.

Lemma muln_iter_r0 a : muln_iter a 0 == 0.
Proof using Type.
    by [].
Qed.

Lemma muln_eq0 m n : (m * n == 0) = (m == 0) || (n == 0).
  Proof using Type.
    case: m => [| m] //.
    case: n => [| n] //.
    rewrite muln0.
    by [].
  Qed.

Lemma orbt b : b || true.
Proof using Type.
  case: b.
  by [].
  by [].
Qed.

Lemma example m p : prime p -> p %| m `! + 1 -> m < p.
Proof using Type.
  move=> prime_p.
  apply: contraLR.
  rewrite -leqNgt.
  move=> leq_p_m.
  rewrite dvdn_addr.
  rewrite gtnNdvd.
    by []. (* ~~ false *)
      by []. (* 0 < 1 *)
        by apply: prime_gt1. (* 1 < p *)
        apply: dvdn_fact.
        rewrite prime_gt0. (* 0 < p <= n *)
          by []. (* true && p <= m *)
            by [].
            (* prime p *)
Qed.

Lemma muln_iter_l0 a : muln_iter 0 a == 0.
Proof using Type.
  elim: a => [// |a IH].
  rewrite IH.
    by [].
Qed.

Lemma addn_assocn a b c : (a + b) + c == a + (b + c).
Proof using Type.
  elim: a => [// |a ih] //.
  Qed.


(* Lemma eqn_to_eq (a b : nat) : a == b -> a = b. *)
(* Proof using Type. *)
(*   elim: a => [// |a iha] //. *)
(* TODO *)

Lemma addn_lS a b : (S a) + b = S (a + b).
Proof using Type.
    by [].
Qed.

Lemma addn_r0 a : a + 0 = a.
Proof using Type.
  elim: a => // a ih //.
  rewrite addn_lS ih //.
Qed.

Lemma addn_rS a b : a + (S b) = S (a + b).
Proof using Type.
  elim: a => [// |a ih] //.
  rewrite !addn_lS ih //.
Qed.

Lemma addn_commut a b : a + b = b + a.
Proof using Type.
  elim: a => [// |a ih] //.
  rewrite addn_r0 //.
  by rewrite addn_lS ih addn_rS.
Qed.

Lemma addn_assoc a b c : (a + b) + c = a + (b + c).
Proof using Type.
  elim: a => [// |a ih] //.
  by rewrite addn_lS !addn_lS ih.
Qed.

Lemma muln_r0 (a : nat) : a * 0 = 0.
Proof using Type.
  elim: a => [] //.
Qed.

Lemma muln_lS (a b : nat) : (S a) * b = b + (a * b).
Proof using Type.
  elim: a => [] //.
Qed.

Lemma muln_rS (a b : nat) : a * (S b) = a + (a * b).
Proof using Type.
  elim: a => [// |a ih] //.
  rewrite muln_lS ih.
  rewrite muln_lS -!addn_assoc (addn_commut (S b) a).
  by rewrite addn_lS addn_rS.
Qed.

Lemma muln_commut a b : a * b = b * a.
Proof using Type.
  elim: a => [// |a ih] //.
  rewrite muln_r0 //.
  by rewrite muln_lS ih muln_rS.
Qed.

(* Exercise 8 *)
Lemma orbA b1 b2 b3 : b1 || (b2 || b3) = b1 || b2 || b3.
Proof using Type.
  elim b1 => [] //.
Qed.

Lemma implybE a b : (a ==> b) = ~~ a || b.
Proof using Type.
  elim a => [] //.
Qed.

Lemma negb_and (a b : bool) : ~~ (a && b) = ~~ a || ~~ b.
Proof using Type.
  elim a => [] //.
Qed.

Locate "a ^ b" .

(* Exercise 9 *)
Lemma subn_sqr m n : m ^ 2 - n ^ 2 = (m - n) * (m + n).
Proof using Type.
    (* by rewrite mulnBl !mulnDr addnC [m * _]mulnC subnDl !mulnn. *)
    rewrite mulnBl !mulnDr.
    rewrite addnC.
    rewrite [m * _]mulnC.
    rewrite subnDl.
    rewrite !mulnn //.
Qed.


(* Exercise 10 *)
Lemma odd_exp m n : odd (m ^ n) = (n == 0) || odd m.
Proof using Type.
  elim: n => // n IHn.
  rewrite expnS.
  rewrite odd_mul.
  rewrite IHn.
  rewrite orKb //.
Qed.

Definition all_words (n : nat) {T : Type} (alphabet : seq T) :=
  let prepend x wl := [seq x :: w | w <- wl] in
  let extend wl := flatten [seq prepend x wl | x <- alphabet] in
  iter extend n [:: [::] ].

(* Lemma size_all_words (n : nat) {T : Type} (alphabet : seq T) : *)
(*   size (all_words n alphabet) = size alphabet ^ n. *)
(* Proof. *)
(*   elim: n => [|n IHn]; first by rewrite expn0. *)
(*   rewrite expnS -{}IHn [in LHS]/all_words iterS -/(all_words _ _). *)
(*   elim: alphabet (all_words _ _) => //= w ws IHws aw. *)
(*     by rewrite size_cat IHws size_map mulSn. *)
(* Qed. *)

