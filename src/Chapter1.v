From mathcomp Require Import all_ssreflect.
(* Set Implicit Arguments. *)
(* Unset Strict Implicit. *)
(* Unset Printing Implicit Defensive. *)

Definition repeat_twice (g : nat -> nat) : nat -> nat :=
  fun x => g (g x).

Definition h (n : nat) : nat -> nat := fun m => n + m * 2.

About h.

Lemma test {a b c : nat} {D:Type} : (a <= b) -> (b <= c) -> D.
Proof.
  move => x y.

Definition three_patterns n :=
  match n with
    u.+1.+1.+1.+1.+1 => u
  | (S v) => v
  | 0 => n
  end.

Eval compute in three_patterns 3.

Fixpoint myaddn n m :=
  match n with
  | 0 => m
  | p.+1 => (myaddn p m).+1
  end.

Eval compute in addn 3 5.

Inductive list (A : Type) : Type :=
| nil : list A
| cons : A -> list A -> list A.

Arguments nil {A}.
Arguments cons {A} a l.

About cons.
Definition l := cons 8 (cons 6 nil).

Fixpoint size {A : Type} (s : list A) :=
  match s with
    | nil => O
    | (cons _ l) => S (size l)
  end.

Print Implicit size.

Fixpoint map {A B : Type} (f : A -> B) s :=
  match s with
    | nil => nil
    | (cons a l) => cons (f a) (map f l)
  end.

(* Inductive reflectl (P : Prop) b := *)
(*  | RT (p : P) (e : b = true) *)
(*  | RF (p : ~ P) (e : b = false). *)
(* About reflect. *)

(* Lemma andP (b1 b2 : bool) : reflectl (b1 /\ b2) (b1 && b2). *)
(* Proof. *)
(*   (*D*)case: b1; case: b2. *)
(*   constructor => //. *)
(*   apply: RF. *)
(*   move => [l r] //. *)
(*   simpl => //. *)

(*   [ left | right => //= [[l r]] ..]. *)
(* Qed. *)

Lemma andP' (b1 b2 : bool) : reflect (b1 /\ b2) (b1 && b2).
Proof.
  (*D*)case: b1; case: b2; [ left | right => //= [[l r]] ..].
Qed.

Inductive pair (A B : Type) : Type := mk_pair (a : A) (b : B).
Notation "( a , b )" := (mk_pair a b) (at level 0, right associativity).
Notation "A × B" := (pair A B) (at level 75, right associativity).


(* Exercise 1 *)
Record Triple (A B C : Type) : Type :=
  mk_triple {
      fst : A
      ; snd : B
      ; thrd : C
    }.
Arguments fst {A B C} t.
Arguments snd {A B C} t.
Arguments thrd {A B C} t.
Arguments mk_triple {A B C} fst snd thrd.

Notation "( a , b , c )" := (mk_triple a b c).
Notation "p .fst" := (fst p) (at level 2).
Notation "p .snd" := (snd p) (at level 2).
Notation "p .thrd" := (thrd p) (at level 2).

Definition l1 : Triple nat nat nat := mk_triple 1 2 3.
Compute (32 , true , 4).thrd.

Fixpoint iter {A : Type} (f : A -> A) (n : nat) (a : A) : A :=
  match n with
    | 0 => a
    | (S n) => iter f n (f a)
  end.

(* Exercise 2 *)
Definition addn_iter (a b : nat) : nat := iter S a b.

(* Exercise 3 *)
Definition muln_iter (a b : nat) : nat := iter (addn a) b 0.
